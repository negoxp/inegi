<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
  <div class="carousel-inner">

      @foreach ($codes as $key => $code)
        <div class="carousel-item {{$key==0 ? 'active' : ''}}">
          <div class="card">
              <div class="card-header">

                <center>
                  <span class="badge badge-pill badge-info">{{$key+1}}</span> <h3>CODIGO: {{$code->codigo}}</h3> 
                </center>
            </div>
            <div class="card-body" style="padding-left: 80px;">
              <h5 class="card-title">GRUPO 1</h5>
              <p class="card-text">{{$code->grupo1}}</p>
              <h5 class="card-title">GRUPO 2</h5>
              <p class="card-text">{{$code->grupo2}}</p>
              <h5 class="card-title">GRUPO 3</h5>
              <p class="card-text">{{$code->grupo3}}</p>
              <h5 class="card-title">GRUPO 4</h5>
              <p class="card-text">{{$code->grupo4}}</p>
              <h5 class="card-title">DESCRIPCION</h5>
              <p class="card-text">{{$code->descripcion}}</p>
              <!--
              <a href="#" class="btn btn-primary">Go somewhere</a>
            -->
            </div>
          </div>
        </div>
      @endforeach

  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"><i class="fas fa-angle-left" style="color:#f0f; font-size:50px;"></i></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">

    <span class="carousel-control-next-icon" aria-hidden="true"><i class="fas fa-angle-right" style="color:#f0f; font-size:50px;"></i></span>
    <span class="sr-only">Next</span>
  </a>
</div>
