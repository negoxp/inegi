@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">

                  <div class="input-group mb-3">
                    <input type="text" class="form-control inputSearch" placeholder="Buscar..." aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary btnSearch" type="button"><i class="fas fa-search"></i></button>
                    </div>
                  </div>

                </div>

                <div class="card-body" style="padding: 5px;">

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$('document').ready(function(){

    $('.btnSearch').click(function(){
      event.preventDefault();
      var search = $('.inputSearch').val();
      var url = '{{route('search')}}/'+search
      if (search!=''){
        $.ajax({
              type: 'GET',
              url: url,
              success: function(response) {
                  $(".card-body").html(response);
                  //console.log(response);
              }
          });
        }
        return false;
    });

    $('.inputSearch').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            $(this).blur();
            $('.btnSearch').click();//Trigger search button click event
        }
    });

});
</script>

@endsection
